﻿using CoreViewTester.CoreView.DeviceController.DeviceEventArgs;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewTester.CoreView.DeviceController.ReceivingData
{
    public class DataReceiver
    {
        byte[] Ok_Response = { 79, 75, 13, 10};

        private SerialPort DataPort = null;

        public bool IsStopTransfer { get; set; } = true;

        public event EventHandler<ReceivingResultEventArgs> DataReceiverEvent;
        private void OnDataSenderEvent(ReceivingResultEventArgs e)
        {
            EventHandler<ReceivingResultEventArgs> handler = DataReceiverEvent;
            if (handler != null)
                handler(this, e);
        }

        public DataReceiver(SerialPort dataPort)
        {
            this.DataPort = dataPort;
            this.IsStopTransfer = false;
            DataWaiterTask();
        }

        private async void DataWaiterTask()
        {
            await Task.Run(() =>
            {
                while (!IsStopTransfer)
                {
                    if (this.DataPort.IsOpen && this.DataPort.BytesToRead > 0)
                    {
                        byte[] buffer = new byte[this.DataPort.BytesToRead];
                        this.DataPort.Read(buffer, 0, buffer.Length);
                        if (buffer.Length < 10)
                        {
                            if (IncludeSubarray(buffer, Ok_Response))
                                if (buffer.Length == Ok_Response.Length)
                                    OnDataSenderEvent(new ReceivingResultEventArgs() { ReceiverState = ReceiverState.Ok });
                                else
                                    OnDataSenderEvent(new ReceivingResultEventArgs() { ReceiverState = ReceiverState.Warning, Discription = "Мусор в ответе" });
                        }
                        else
                        {
                            OnDataSenderEvent(new ReceivingResultEventArgs() { 
                                ReceiverState = ReceiverState.Data, 
                                Discription = "Данные",
                                Data = buffer
                            });
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Метод поиска подмассива в массиве. TODO - есть другой метод
        /// </summary>
        /// <param name="array"></param>
        /// <param name="subarray"></param>
        /// <returns></returns>
        private bool IncludeSubarray(byte[] array, byte[] subarray)
        {
            bool isEqual = true;
            for (int i=0; i < array.Length - subarray.Length + 1; i++)
            {
                isEqual = true;
                for (int j=0; j < subarray.Length; j++)
                {
                    if (subarray[j] != array[i + j])
                    {
                        isEqual = false;
                        break;
                    }
                }
                if (isEqual)
                    return true;
            }
            return false;
        }

    }
}
