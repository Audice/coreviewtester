﻿using CoreViewTester.CoreView.ConnectPart;
using CoreViewTester.CoreView.DeviceController.DeviceEventArgs;
using CoreViewTester.CoreView.DeviceController.ReceivingData;
using CoreViewTester.CoreView.DeviceController.SendData;
using CoreViewTester.CoreView.Settings;
using CoreViewTester.CoreView.SignalsHandler.SignalsEventArgs;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewTester.CoreView.DeviceController
{
    public class CoreViewDevice : IDisposable
    {
        /// <summary>
        /// Объект поиска устройств CoreView. Необходим для контроля переподключения
        /// </summary>
        private DeviceDetector DeviceDetector;

        /// <summary>
        /// Порт данных от устройства CoreView
        /// </summary>
        private SerialPort CoreViewDataPort = null;

        /// <summary>
        /// Состояние подключения устройства CoreView в программе
        /// </summary>
        public bool DeviseConnected { get; private set; } = false;


        public event EventHandler<SerialPortStateEventArgs> SerialDataStateAction;

        /// <summary>
        /// Объект, отвечающий за отправку данных по порту
        /// </summary>
        private DataSender DataSender = null;

        private DataReceiver DataReceiver = null;

        private Queue<Tuple<Commands, byte>> CommandsQueue = null;//Очередь реальных команд

        private void OnSerialChange(SerialPortStateEventArgs e)
        {
            EventHandler<SerialPortStateEventArgs> handler = SerialDataStateAction;
            if (handler != null)
                handler(this, e);
        }


        public event EventHandler<RawSignalsEventArgs> RawDataReadyEvent;

        private void OnRawDataSend(RawSignalsEventArgs e)
        {
            EventHandler<RawSignalsEventArgs> handler = RawDataReadyEvent;
            if (handler != null)
                handler(this, e);
        }
        

        private BaseSettings Settings { get; set; } = null;
        private bool SettingModeOn = false;

        public CoreViewDevice(string port, BaseSettings baseSettings)
        {
            this.Settings = baseSettings;
            this.CommandsQueue = new Queue<Tuple<Commands, byte>>();
            this.CoreViewDataPort = new SerialPort() { BaudRate = 115200, Parity = Parity.None, DataBits = 8, StopBits = StopBits.One };
            this.CoreViewDataPort.ErrorReceived += ErrorPortOccurred;
            this.DataSender = new DataSender(this.CoreViewDataPort);
            this.DataReceiver = new DataReceiver(this.CoreViewDataPort);
            this.DataReceiver.DataReceiverEvent += ReceivedDataHandler;
            this.DataSender.DataSenderEvent += DataSenderEventHandler;
            //
            OpenDataPort(port);

            //
            this.SettingModeOn = true;
            StartSetting(this.Settings.CalibrValue, this.Settings.ChannelsNum);
        }



        private bool StartSetting(byte calibrValue, byte channelsNum)
        {
            //1. Установка стартовых команд в очередь
            this.CommandsQueue.Enqueue(new Tuple<Commands, byte>(Commands.Calibration, calibrValue));
            this.CommandsQueue.Enqueue(new Tuple<Commands, byte>(Commands.SetChannelsNum, channelsNum));

            if (this.DataSender.СalibrationCommand(calibrValue))
            {
                return true;
            }
            return false;
        }

        private void ReceivedDataHandler(object sender, ReceivingResultEventArgs e)
        {
            //Если активен режим стартовой настройки
            if (this.SettingModeOn)
            {
                switch (e.ReceiverState)
                {
                    case ReceiverState.Ok:
                        this.SendCurrentCommand();
                        break;
                    case ReceiverState.Warning:
                        break;
                    case ReceiverState.Error:
                        break;
                }
                //Если число команд в очереди равно 0 и не было ошибок,
                //то добавляем команду запроса данных в очеред и запускаем процесс постоянного получения данных
                if (this.CommandsQueue.Count == 0)
                {
                    this.CommandsQueue.Enqueue(new Tuple<Commands, byte>(Commands.GetData, 0));
                    this.SettingModeOn = false;
                    this.SendCurrentCommand();
                }
            }
            else //Иначе запускаем постоянный опрос
            {
                switch (e.ReceiverState)
                {
                    case ReceiverState.Ok:
                        //this.CommandsQueue.Enqueue(new Tuple<Commands, byte>(Commands.GetData, 0));
                        //this.SendCurrentCommand();
                        break;
                    case ReceiverState.Data:
                        //Обработка данных - перенаправление
                        this.OnRawDataSend(new RawSignalsEventArgs() { RawData = e.Data });
                        //Запрос новой порции
                        this.CommandsQueue.Enqueue(new Tuple<Commands, byte>(Commands.GetData, 0));
                        this.SendCurrentCommand();
                        break;
                }
            }
        }

        /// <summary>
        /// Метод отправки команды, стоящей первой в очереди
        /// </summary>
        private void SendCurrentCommand()
        {
            if (this.CommandsQueue.Count > 0)
            {
                Tuple<Commands, byte> currentCommand = this.CommandsQueue.Dequeue();
                switch (currentCommand.Item1)
                {
                    case Commands.Calibration:
                        this.DataSender.СalibrationCommand(currentCommand.Item2);
                        break;
                    case Commands.SetChannelsNum:
                        this.DataSender.SetChannelsNum(currentCommand.Item2);
                        break;
                    case Commands.GetData:
                        this.DataSender.GetDataCommand();
                        break;
                }
            }
            else
            {
                //Закончились команды...
                int s = 0;
            }
        }

        private void DataSenderEventHandler(object sender, SerialPortStateEventArgs e)
        {
            if (e.PortState == SerialPortState.SuccessfullySent)
            {
                //Обработка хороших комманд
            }
            else
            {
                //Обработка плохих команд
            }
        }

        private bool OpenDataPort(string port)
        {
            this.CoreViewDataPort.PortName = port;
            this.CoreViewDataPort.DtrEnable = true;
            //Если порт открыт - закроем его
            if (this.CoreViewDataPort.IsOpen)
                this.CoreViewDataPort.Close();
            //Открываем соединение по порту port
            try
            {
                this.CoreViewDataPort.Open();
            }
            catch (Exception ex)
            {
                //Подключение завершилось с ошибкой
                return false;
            }

            if (this.CoreViewDataPort.IsOpen)
            {
                //Открытие порта состоялось
                this.CoreViewDataPort.DiscardOutBuffer();
                this.CoreViewDataPort.DiscardInBuffer();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Метод, срабатывающий при появлении ошибки порта данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ErrorPortOccurred(object sender, SerialErrorReceivedEventArgs e)
        {
            //TODO: проверить как работает при дисконекте
            if (this.CoreViewDataPort.IsOpen)
            {

            }
            else
            {

            }
        }

        /// <summary>
        /// Реакция на событие от порта данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void DataPortEventOccurred(object sender, SerialPortStateEventArgs e)
        {
            switch (e.PortState)
            {
                case SerialPortState.BreakConnect:
                    break;
                case SerialPortState.ClosedClient:
                    break;
                default:
                    break;
            }
        }

        public void Dispose()
        {
            this.DataSender.DataSenderEvent -= DataSenderEventHandler; //??
            this.DataReceiver.DataReceiverEvent -= ReceivedDataHandler;
        }
    }
}
