﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewTester.CoreView.DeviceController.DeviceEventArgs
{
    public class ReceivingResultEventArgs : EventArgs
    {
        public ReceiverState ReceiverState { get; set; }
        public string Discription { get; set; }
        public byte[] Data { get; set; }
    }
}
