﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewTester.CoreView.DeviceController.DeviceEventArgs
{
    public class SerialPortStateEventArgs : EventArgs
    {
        /// <summary>
        /// Состояние порта данных
        /// </summary>
        public SerialPortState PortState { get; set; }
        /// <summary>
        /// Текущая команда
        /// </summary>
        public Commands CurrentCommand { get; set; }
        /// <summary>
        /// Описание состояния 
        /// </summary>
        public string Discription { get; set; }
        /// <summary>
        /// Реально установленное значение команды
        /// </summary>
        public byte CommandValue { get; set; }
    }
}
