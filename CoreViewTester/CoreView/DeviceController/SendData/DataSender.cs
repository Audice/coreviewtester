﻿using CoreViewTester.CoreView.DeviceController.DeviceEventArgs;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewTester.CoreView.DeviceController.SendData
{
    public class DataSender
    {
        private SerialPort DataPort = null;

        public event EventHandler<SerialPortStateEventArgs> DataSenderEvent;

        private void OnDataSenderEvent(SerialPortStateEventArgs e)
        {
            EventHandler<SerialPortStateEventArgs> handler = DataSenderEvent;
            if (handler != null)
                handler(this, e);
        }

        public DataSender(SerialPort dataport)
        {
            this.DataPort = dataport;
        }
        /// <summary>
        /// Команда калибровки устройства
        /// </summary>
        /// <param name="calibValue">Значение калибровки: от 0 до 100</param>
        /// <returns>True - калибровка совершена, false - иначе</returns>
        public bool СalibrationCommand(byte calibValue)
        {
            SerialPortStateEventArgs commandArgs = new SerialPortStateEventArgs() { CurrentCommand = Commands.Calibration, CommandValue = calibValue };
            string sendcommand = "CALIBR " + calibValue.ToString();
            byte[] bytes = Encoding.ASCII.GetBytes(sendcommand);
            try
            {
                this.DataPort.Write(bytes, 0, bytes.Length);
                commandArgs.PortState = SerialPortState.SuccessfullySent; commandArgs.Discription = "Успех.";
            }
            catch (Exception ex)
            {
                commandArgs.PortState = SerialPortState.BreakConnect; commandArgs.Discription = "Произошла ошибка при отправке. " + ex.Message;
            }
            this.OnDataSenderEvent(commandArgs);
            if (commandArgs.PortState == SerialPortState.SuccessfullySent)
                return true;
            return false;
        }

        public bool GetDataCommand()
        {
            SerialPortStateEventArgs commandArgs = new SerialPortStateEventArgs() { CurrentCommand = Commands.GetData };
            byte[] bytes = Encoding.ASCII.GetBytes("GET CMU");
            try
            {
                this.DataPort.Write(bytes, 0, bytes.Length);
                commandArgs.PortState = SerialPortState.SuccessfullySent; commandArgs.Discription = "Успех.";
            }
            catch (Exception ex)
            {
                commandArgs.PortState = SerialPortState.BreakConnect; commandArgs.Discription = "Произошла ошибка при отправке. " + ex.Message;
            }
            this.OnDataSenderEvent(commandArgs);
            if (commandArgs.PortState == SerialPortState.SuccessfullySent)
                return true;
            return true;
        }
        /// <summary>
        /// Команда настройки числа опрашиваемых каналов
        /// </summary>
        /// <param name="channelsNum">Количество каналов: от 1 до 10</param>
        /// <returns>True - в случае отсутствия проблем с отправкой, иначе false</returns>
        public bool SetChannelsNum(byte channelsNum)
        {
            SerialPortStateEventArgs commandArgs = new SerialPortStateEventArgs() { CurrentCommand = Commands.SetChannelsNum, CommandValue = channelsNum };
            string sendcommand = "SET CH " + channelsNum.ToString();
            byte[] bytes = Encoding.ASCII.GetBytes(sendcommand);
            try
            {
                this.DataPort.Write(bytes, 0, bytes.Length);
                commandArgs.PortState = SerialPortState.SuccessfullySent; commandArgs.Discription = "Успех.";
            }
            catch (Exception ex)
            {
                commandArgs.PortState = SerialPortState.BreakConnect; commandArgs.Discription = "Произошла ошибка при отправке. " + ex.Message;
            }
            this.OnDataSenderEvent(commandArgs);
            if (commandArgs.PortState == SerialPortState.SuccessfullySent)
                return true;
            return false;
        }


    }
}
