﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewTester.CoreView
{
    public enum SerialPortState
    {
        BreakConnect, //Соединение сломалось само
        ClosedClient, //Клиент закрыл соединение 
        InvalidCommand, //Неверная команда
        SuccessfullySent //Успешная отправка команды
    }

    public enum Commands
    {
        Calibration,
        SetChannelsNum,
        GetData,
    }

    public enum ReceiverState
    {
        Ok,
        Error,
        Warning,
        Data
    }
}
