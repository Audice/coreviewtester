﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewTester.CoreView.SignalsHandler.SignalsEventArgs
{
    public class RawSignalsEventArgs : EventArgs
    {
        /// <summary>
        /// Набор байт, характеризующий пакет данных
        /// </summary>
        public byte[] RawData { get; set; }
    }
}
