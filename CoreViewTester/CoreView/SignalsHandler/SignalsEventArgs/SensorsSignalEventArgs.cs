﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewTester.CoreView.SignalsHandler.SignalsEventArgs
{
    public class SensorsSignalEventArgs : EventArgs
    {
        public int[] Data { get; set; }
    }
}
