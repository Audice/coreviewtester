﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewTester.CoreView.SignalsHandler.SignalsEventArgs
{
    public class DataPackageEventArgs : EventArgs
    {
        public ushort PackageLength { get; set; }
        public ushort SamplingFrequency { get; set; }
        public Byte NumOfBit { get; set; }
        public Int16 UMV { get; set; }
        public Byte NumOfChenal { get; set; }
        public uint MeasurementNumber { get; set; }
        public int[,] PackageData { get; set; }
    }
}
