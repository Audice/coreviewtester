﻿using CoreViewTester.CoreView.SignalsHandler.SignalsEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewTester.CoreView.SignalsHandler
{
    /// <summary>
    /// Промежуточная обработка данных с использованием фильтров
    /// </summary>
    public class PackageHandler
    {
        /// <summary>
        /// Буфер данных. Используется для фильтрации
        /// </summary>
        private int[,] DataBuffer = null;
        /// <summary>
        /// Флаг установки парамметров обработчика данных пакетов
        /// </summary>
        public bool IsHandlerSet { get; private set; } = false;
        /// <summary>
        /// Заполнен ли буфер данных
        /// </summary>
        public bool IsBufferFill { get; private set; } = false;

        /// <summary>
        /// Количество каналов. Перед установкой равен 0 и не валиден.
        /// </summary>
        public byte ChannelsNum { get; private set; } = 0;
        /// <summary>
        /// Размер окна фильтра. По дефолту равен 0 и не валиден
        /// </summary>
        public byte FilterWindowsSize { get; private set; } = 0;
        /// <summary>
        /// Номер последноего добавленного пакета
        /// </summary>
        public uint LastPackageNumber { get; private set; } = 0;
        /// <summary>
        /// Индекс для отслеживания места в буфере
        /// </summary>
        private uint BufferIndex = 0;


        public event EventHandler<SensorsSignalEventArgs> SensorsSignalReady;
        private void OnSensorsSignalReady(SensorsSignalEventArgs e)
        {
            EventHandler<SensorsSignalEventArgs> handler = SensorsSignalReady;
            if (handler != null)
                handler(this, e);
        }


        public PackageHandler(byte sizeBuffer = 5)
        {
            this.FilterWindowsSize = sizeBuffer;
        }



        public void AppendPackage(DataPackageEventArgs package)
        {
            if (!this.IsHandlerSet)
            {
                this.SetSessionParametrs(package.NumOfChenal);
            }
            if (this.PushDataPart(package.PackageData))
            {
                this.SendOutData(DataPreparation());
            }
        }

        private void SendOutData(int[] data)
        {
            SensorsSignalEventArgs e = new SensorsSignalEventArgs()
            {
                Data = data
            };
            OnSensorsSignalReady(e);
        }

        private int[] DataPreparation()
        {
            int[] resultArray = new int[this.ChannelsNum];
            uint lastIndex = this.IsBufferFill ? this.FilterWindowsSize : this.BufferIndex;
            for (int i = 0; i < this.DataBuffer.GetLength(1); i++)
            {
                for (int j = 0; j < lastIndex; j++)
                {
                    resultArray[i] += this.DataBuffer[j, i];
                }
                resultArray[i] = (int)((double)resultArray[i] / lastIndex);
            }
            return resultArray;
        }

        public bool PushDataPart(int[,] data)
        {
            if (data != null && this.DataBuffer != null && data.GetLength(1) == this.DataBuffer.GetLength(1))
            {
                for (int i=0; i < data.GetLength(0); i++)
                {
                    for (int j = 0; j < data.GetLength(1); j++)
                    {
                        this.DataBuffer[this.BufferIndex, j] = data[i, j];
                    }
                    this.BufferIndex = (this.BufferIndex + 1) % this.FilterWindowsSize;
                }

                if (!this.IsBufferFill && this.BufferIndex == this.FilterWindowsSize - 1)
                    this.IsBufferFill = true;

                return true;
            }
            else
            {
                //Обработка несоответствия
                //Разные наборы данных. Необходимо сбросить старые данные
                //TODO
                if (data != null && this.DataBuffer != null && data.GetLength(1) != this.DataBuffer.GetLength(1))
                {
                    this.IsHandlerSet = false;
                }
            }
            return false;
        }

        private void SetSessionParametrs(byte channelsNum)
        {
            this.IsHandlerSet = true;
            this.ChannelsNum = channelsNum;
            this.DataBuffer = new int[this.FilterWindowsSize, channelsNum];
            this.BufferIndex = 0;
        }



    }
}
