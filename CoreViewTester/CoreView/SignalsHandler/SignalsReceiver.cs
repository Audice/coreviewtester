﻿using CoreViewTester.CoreView.DeviceController;
using CoreViewTester.CoreView.SignalsHandler.SignalsEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewTester.CoreView.SignalsHandler
{
    /// <summary>
    /// Класс, подписывающийся на рассылку пакетов с данными сигнала
    /// </summary>
    public class SignalsReceiver : IDisposable
    {
        private readonly byte[] EcgDataTemplate = new byte[] { 67, 77, 85, 32, 68, 65, 84, 65, 34 };
        private readonly byte[] EndTemplate = new byte[] { 34, 13, 10 };

        private byte PackageHeaderSize = 11;

        private CoreViewDevice Device = null;
        private Queue<byte> RawDataBuffer = null;
        private const uint HandleBound = 70;
        /// <summary>
        /// Список, предназначенный для временного хранения пакетных байт
        /// </summary>
        private List<byte> ResponseList = null;




        public event EventHandler<DataPackageEventArgs> DataPackageFinished;
        private void OnDataPackageFinished(DataPackageEventArgs e)
        {
            EventHandler<DataPackageEventArgs> handler = DataPackageFinished;
            if (handler != null)
                handler(this, e);
        }
        /// <summary>
        /// Обработчик пакетов с днными...
        /// </summary>
        public PackageHandler PackageHandler { get; private set; } = null;

        public SignalsReceiver(CoreViewDevice device)
        {
            if (device == null)
                throw new Exception("Не определён девайс типа CoreViewDevice");

            this.PackageHandler = new PackageHandler();

            this.RawDataBuffer = new Queue<byte>();
            this.ResponseList = new List<byte>();
            this.Device = device;
            //TODO: попробуем два варианта приёма и обработки пакетов от CoreViewDevice
            //1. Приём и обработка потока данных последовательно, по приходу новой части
            this.Device.RawDataReadyEvent += RawDataHandler;
            //2. Второй метод - приём новой части и обработка потока в параллельной таске
        }

        /// <summary>
        /// Метод, срабатывающий при оступлении новых, сырых данных
        /// </summary>
        /// <param name="sender">Объект-отправитель данных</param>
        /// <param name="e">Сами данные</param>
        private void RawDataHandler(object sender, RawSignalsEventArgs e)
        {
            //Копируем данные в локальный буфер
            foreach (byte b in e.RawData)
                this.RawDataBuffer.Enqueue(b);
            //При достижении минимального размера пакета запускаем парсер сырых байт
            SearchPackage();
        }

        private void SearchPackage()
        {
            while (this.RawDataBuffer.Count > 0)
            {
                this.ResponseList.Add(this.RawDataBuffer.Dequeue());

                //поиска загаловка GET CMU
                int startIndexECGTemplate = SearchTemplate(this.ResponseList, this.EcgDataTemplate);
                int removeNum = ECGPackagePreparation(startIndexECGTemplate, this.ResponseList);

                if (startIndexECGTemplate >= 0 && removeNum > 0)
                {
                    this.ResponseList.RemoveRange(0, startIndexECGTemplate + removeNum);
                    GC.Collect();
                }
            }
        }

        private int SearchTemplate(List<byte> responseList, byte[] ecgDataTemplate)
        {
            if (responseList.Count >= ecgDataTemplate.Length)
            {
                for (int i = 0; i <= responseList.Count - ecgDataTemplate.Length; i++)
                {
                    bool isSearched = true;
                    for (int j = 0; j < ecgDataTemplate.Length; j++)
                    {
                        if (responseList[i + j] != ecgDataTemplate[j])
                        {
                            isSearched = false;
                            break;
                        }
                    }
                    if (isSearched)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        private int ECGPackagePreparation(int startTemplateIndex, List<byte> rawECGData)
        {
            if (rawECGData == null) return -1;
            if (startTemplateIndex >= 0 && (rawECGData.Count - startTemplateIndex) > HandleBound)
            {
                int startIndex = startTemplateIndex + this.EcgDataTemplate.Length;
                //Удаление старых данных
                //responseList.RemoveRange(startIndexECGTemplate, this.EcgDataTemplate.Length);

                List<byte> ecgPackage = new List<byte>();
                //Если мы нашли заголовог ECG DATA
                for (int i = 0; i < PackageHeaderSize; i++)
                {
                    ecgPackage.Add(rawECGData[startIndex + i]);
                }
                startIndex += PackageHeaderSize;

                ushort packageLength = BitConverter.ToUInt16(new byte[] { ecgPackage[0], ecgPackage[1] }, 0);
                ushort samplingFrequency = BitConverter.ToUInt16(new byte[] { ecgPackage[2], ecgPackage[3] }, 0);
                byte numOfBit = ecgPackage[4];
                short umv = BitConverter.ToInt16(new byte[] { ecgPackage[5], ecgPackage[6] }, 0);
                byte numOfChenal = ecgPackage[7];
                uint measurementNumber = (uint)(ecgPackage[8] + ecgPackage[9] * 256 + ecgPackage[10] * 256 * 256);
                //После получения заголовка, считываем данные
                if (packageLength <= 0)
                {
                    return -1; //Если длина пакета не валидна - выходим из цикла
                }
                int dataLength = packageLength - this.PackageHeaderSize;
                if (dataLength <= 0)
                {
                    return -1; //Если длина данных не валидна - выходим из цикла
                }

                ecgPackage.Clear();
                for (int i = 0; i < dataLength; i++)
                {
                    ecgPackage.Add(rawECGData[startIndex + i]);
                }
                startIndex += dataLength;

                byte[] ecgPackageData = ecgPackage.ToArray();
                ecgPackage.Clear();
                //Проверка конца пакета на корректность
                for (int i = 0; i < EndTemplate.Length; i++)
                {
                    ecgPackage.Add(rawECGData[startIndex + i]);
                }
                startIndex += EndTemplate.Length;

                if (ecgPackage[0] == this.EndTemplate[0]
                && ecgPackage[1] == this.EndTemplate[1]
                && ecgPackage[2] == this.EndTemplate[2])
                {
                    var parsedData = ParsePackageData(ecgPackageData, numOfChenal, numOfBit, packageLength);
                    DataPackageEventArgs packet = new DataPackageEventArgs()
                    {
                        MeasurementNumber = measurementNumber,
                        NumOfBit = numOfBit,
                        NumOfChenal = numOfChenal,
                        PackageData = parsedData,
                        PackageLength = packageLength,
                        SamplingFrequency = samplingFrequency,
                        UMV = umv
                    };
                    this.PackageHandler.AppendPackage(packet);

                }

                ecgPackage.Clear();
                ecgPackage = null;
                return startIndex - startTemplateIndex;
            }
            return -1;
        }

        private int[,] ParsePackageData(byte[] SignalsArray, byte channelsNum, byte numOfBit, ushort packageLength)
        {
            int[,] packageData = null;
            if (channelsNum > 0)
            {
                if (packageData == null || (packageLength - 11) != SignalsArray.Length)
                {
                    packageData = new int[((SignalsArray.Length) / channelsNum) / (numOfBit / 8), channelsNum];
                    List<int> tmpRealValueList = new List<int>();
                    for (int i = 0; i < SignalsArray.Length; i += 3)
                    {
                        tmpRealValueList.Add((int)(SignalsArray[i] + SignalsArray[i + 1] * 256 + SignalsArray[i + 2] * 256 * 256));
                        //tmpRealValueList.Add(BitConverter.ToInt32(new byte[] { SignalsArray[i], SignalsArray[i + 1], SignalsArray[i + 2] }, 0));
                    }
                    for (int i = 0; i < tmpRealValueList.Count; i++)
                    {
                        packageData[i / channelsNum, i % channelsNum] = tmpRealValueList[i];
                    }
                }
                else
                {
                    List<int> tmpRealValueList = new List<int>();
                    for (int i = 0; i < SignalsArray.Length; i += 3)
                    {
                        //tmpRealValueList.Add(BitConverter.ToInt16(new byte[] { SignalsArray[i], SignalsArray[i + 1] }, 0));
                        tmpRealValueList.Add((int)(SignalsArray[i] + SignalsArray[i + 1] * 256 + SignalsArray[i + 2] * 256 * 256));
                    }
                    for (int i = 0; i < tmpRealValueList.Count; i++)
                    {
                        packageData[i / channelsNum, i % channelsNum] = tmpRealValueList[i];
                    }
                }
            }
            else
                packageData = null;

            return packageData;

        }

        public void Dispose()
        {
            this.Device.RawDataReadyEvent -= RawDataHandler;
        }
    }
}
