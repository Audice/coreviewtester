﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewTester.CoreView.Settings
{
    /// <summary>
    /// Класс хрнения и контроля настроек
    /// </summary>
    public class BaseSettings
    {
        private byte calibrValue = 0;
        /// <summary>
        /// Поле для хранения значения калибровки с контролем установки значения: от 0 до 100
        /// </summary>
        public byte CalibrValue { 
            get
            {
                return calibrValue;
            }
            set
            {
                calibrValue = value;
                if (calibrValue > 100)
                    calibrValue = 100;
            }
        }

        private byte channelsNum = 0;
        /// <summary>
        /// Поле для хранения количества каналов устройства: от 1 до 10
        /// </summary>
        public byte ChannelsNum
        {
            get
            {
                return channelsNum;
            }
            set
            {
                channelsNum = value;
                if (channelsNum > 10)
                    channelsNum = 10;
                if (channelsNum < 1)
                    channelsNum = 1;
            }
        }

        public BaseSettings(byte _calibrValue, byte _channelsNum)
        {
            this.CalibrValue = _calibrValue;
            this.ChannelsNum = _channelsNum;
        }
    }
}
