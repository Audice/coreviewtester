﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewTester.CoreView.Settings
{
    /// <summary>
    /// Класс обработки настроек
    /// </summary>
    public class SettingHandler
    {
        public BaseSettings BaseSettings { get; private set; } = null;
        /// <summary>
        /// Конструктор с дефолтными значениями параметров
        /// </summary>
        /// <param name="calibrValue">Калибровочное значение устройство</param>
        /// <param name="channelsNum">Количество опрашиваемых каналов</param>
        public SettingHandler(byte calibrValue = 70, byte channelsNum = 1)
        {
            this.BaseSettings = new BaseSettings(calibrValue, channelsNum);
        }

        /// <summary>
        /// Конструктор загрузчик для из файла-настроек
        /// </summary>
        public SettingHandler(string path)
        {

        }

    }
}
