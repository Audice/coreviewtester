﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Management;

namespace CoreViewTester.CoreView.ConnectPart
{
    public class DeviceDetector
    {
        /// <summary>
        /// Список устройств по идентификатору
        /// </summary>
        public List<string> COMsNames { get; private set; } = null;
        /// <summary>
        /// Идентификатор устройтсв
        /// </summary>
        private const string PNPDeviceID = "COREVIEW_CDC";

        public DeviceDetector()
        {
            this.COMsNames = FindDevices();
        }

        /// <summary>
        /// Метод поиска устройства в системе
        /// </summary>
        /// <returns>Возвращает список ком портов если они есть. Иначе null</returns>
        private List<string> FindDevices()
        {
            List<string> COMs = new List<string>();
            ManagementObjectSearcher query = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_SerialPort");
            ManagementObjectCollection queryCollection = query.Get();
            foreach (ManagementObject mo in queryCollection)
                if ((mo["PNPDeviceID"].ToString()).Contains(PNPDeviceID))
                    COMs.Add(mo["DeviceID"].ToString());
            return COMs.Count > 0 ? COMs : null;
        }

        /// <summary>
        /// Проверка исходного устройтсва среди имеющихся на данный момент
        /// </summary>
        /// <returns>true, если устройство есть в системе. Иначе false</returns>
        public bool PortAvailabilityCheck(string curPortName)
        {
            List<string> comDev = FindDevices();
            return comDev.Contains(curPortName);
        }


    }
}
