﻿using CoreViewTester.CoreView.ConnectPart;
using CoreViewTester.CoreView.DeviceController;
using CoreViewTester.CoreView.Settings;
using CoreViewTester.CoreView.SignalsHandler;
using CoreViewTester.CoreView.SignalsHandler.SignalsEventArgs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoreViewTester
{
    public partial class Form1 : Form
    {
        CoreViewDevice CoreViewDevice;
        SettingHandler SettingsHandler = null;
        SignalsReceiver signalsReceiver = null;
        public Form1()
        {
            InitializeComponent();
            
            DeviceDetector dd = new DeviceDetector();
            this.CVDevices.DataSource = dd.COMsNames;
            this.SettingsHandler = new SettingHandler();

        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            string selectedItem = this.CVDevices.SelectedItem.ToString();
            if (selectedItem != null && selectedItem.Length > 0)
            {
                CoreViewDevice = new CoreViewDevice(selectedItem, this.SettingsHandler.BaseSettings);
                signalsReceiver = new SignalsReceiver(CoreViewDevice);
                signalsReceiver.PackageHandler.SensorsSignalReady += fillData;
            }
                
        }

        private void fillData(object sender, SensorsSignalEventArgs e)
        {
            if (e.Data != null)
                this.label2.Invoke(new Action(() => {
                    this.label2.Text = "";
                    for (int i = 0; i < e.Data.Length; i++)
                    {
                        this.label2.Text += e.Data[i].ToString() + System.Environment.NewLine;
                    }
                }));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }
    }
}
